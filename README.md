# Node excel-to-json-data

A node microservice block for [kintohub](http://kintohub.com) to extra data from excel files
and return it in json format. Works with .xls, .xlsx and .csv.


# Entrypoints

See ApiDoc


# Local development

* docker-compose run -d


# Support

http://www.kintohub.com
