const express = require('express')
const multer  = require('multer')
const upload = multer({ dest: 'uploads/' })
const excelToJson = require('convert-excel-to-json');

const app = express()
const PORT = process.env.PORT || '8000'


/**
 * @api {post} /excel hello world sample request
 * @apiName PostExcel
 * @apiGroup Excel
 * 
 * @apiparam {File} excel Form-based File Upload in HTML
 * 
 * @apiSuccess (Success_200) {Object} data json representation of the excel file
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "data": {
 *         "Worksheet 1": [
 *           { "A": "Name", "B: "Title" },
 *           { "A": "John Doe", "B": "Founder" },
 *           { "A": "Jane Dear", "B": "CEO" }
 *         ]
 *       }
 *     }
 */
app.post('/excel', upload.single('excel'), function (req, res, next) {
  // req.file is the `excel` file
  // req.body will hold the text fields, if there were any
  const result = excelToJson({
    sourceFile: req.file.path
  });

  res.send({ data: result });
})

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`))
